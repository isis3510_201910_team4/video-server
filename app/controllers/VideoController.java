package controllers;

import com.coremedia.iso.IsoFile;
import com.google.cloud.storage.*;
import com.google.gson.Gson;
import com.googlecode.mp4parser.FileDataSourceImpl;
import controllers.base.BaseController;
import okhttp3.*;
import org.apache.commons.io.FileUtils;
import play.mvc.Http;
import play.mvc.Result;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ThreadLocalRandom;

public class VideoController extends BaseController {

    public Result receiveNewVideo(String userId, String reportId){
        try {
            Http.MultipartFormData<File> body = request().body().asMultipartFormData();
            Http.MultipartFormData.FilePart<File> video = body.getFile("video");
            if (video == null)
                throw new Exception("No video was received on out end");

            String contentType = video.getContentType();
            File videoFile = video.getFile();

            CompletableFuture.runAsync(() -> {
                processVideo(contentType, userId, reportId, videoFile);
            });
            return ok("Video received, scheduled for processing");
        } catch (Exception e){
            e.printStackTrace();
            return error(e.getMessage());
        }
    }

    private void processVideo(String contentType, String userId, String reportId, File videoFile){
        try {
            Storage storage = StorageOptions.getDefaultInstance().getService();
            BlobId blobId = BlobId.of("mobile-videos", System.currentTimeMillis()+"-video");
            BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType(contentType).build();
            Blob blob = storage.create(blobInfo, FileUtils.readFileToByteArray(videoFile));

            int videoDuration = (int) getVideoDuration(videoFile);
            int amountOfRecords = videoDuration/AMOUNT_SECONDS_PER_RECORD;
            double [] records = new double[amountOfRecords];
            for (int i = 0; i < amountOfRecords; i++) {
                records[i] = ThreadLocalRandom.current().nextDouble(60, 101);
            }

            updateReportDatabase(DATABASE_URL+"reports/"+userId+"/"+reportId+".json", records);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private void updateReportDatabase(String url, double [] data) throws Exception{
        Gson gson = new Gson();
        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put("processed", true);
        objectMap.put("heartRate", data);
        objectMap.put("averageHeartRate", Arrays.stream(data).sum()/data.length);

        OkHttpClient client = new OkHttpClient();

        RequestBody requestBody = RequestBody.create(JSON, gson.toJson(objectMap));

        Request request = new Request.Builder()
                .url(url+"?access_token="+ACCESS_TOKEN)
                .patch(requestBody)
                .build();
        try (Response response = client.newCall(request).execute()) {
            response.body().string();
        }
    }

    private double getVideoDuration (File video) throws IOException {
        double videoDuration = 0;
        //pathPlaying is a string this format: "src/videos/video.mp4"
        IsoFile isoFile = new IsoFile(new FileDataSourceImpl(video));
        videoDuration = (double)
                isoFile.getMovieBox().getMovieHeaderBox().getDuration() /
                isoFile.getMovieBox().getMovieHeaderBox().getTimescale();
        return videoDuration;
    }


}
