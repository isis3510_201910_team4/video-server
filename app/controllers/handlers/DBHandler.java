//package controllers.handlers;
//
//import com.amazonaws.auth.AWSCredentialsProvider;
//import com.amazonaws.auth.AWSStaticCredentialsProvider;
//import com.amazonaws.auth.BasicAWSCredentials;
//import com.amazonaws.regions.Regions;
//import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
//import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
//import com.amazonaws.services.dynamodbv2.datamodeling.*;
//import com.amazonaws.services.dynamodbv2.model.AttributeValue;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//public class DBHandler {
//
//    private static AWSCredentialsProvider awsCreds = new AWSStaticCredentialsProvider( new BasicAWSCredentials(System.getenv("AWS_ACCESS_KEY"), System.getenv("AWS_ACCESS_SECRET")) );
//    private static Regions awsRegion = Regions.US_EAST_1;
//
//    public static AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard()
//            .withRegion(awsRegion)
//            .withCredentials(awsCreds)
//            .build();
//
//    public void save(Object object) throws Exception{
//        try {
//            DynamoDBMapper mapper = new DynamoDBMapper(client);
//            mapper.save(object);
//        } catch (Exception e){
//            e.printStackTrace();
//            throw e;
//        }
//    }
//
//    public void delete(Object obj) throws Exception{
//        try {
//            DynamoDBMapper mapper = new DynamoDBMapper(client);
//            mapper.delete(obj);
//        } catch (Exception e){
//            e.printStackTrace();
//            throw e;
//        }
//    }
//
//    public <T> T find(Object id, Class<T> clazz){
//        try {
//            DynamoDBMapper mapper = new DynamoDBMapper(client);
//            return clazz.cast(mapper.load(clazz, id));
//        } catch (Exception e){
//            e.printStackTrace();
//            throw e;
//        }
//    }
//
//    public <T> T findOne(String attribute, String value, Class<T> clazz){
//        try {
//            DynamoDBMapper mapper = new DynamoDBMapper(client);
//            Map<String, AttributeValue> eav = new HashMap<>();
//            eav.put(":val", new AttributeValue().withS(value));
//
//            DynamoDBQueryExpression<T> queryExpression = new DynamoDBQueryExpression<T>()
//                    .withKeyConditionExpression(attribute+" = :val")
//                    .withExpressionAttributeValues(eav);
//
//
//            if (attribute.equalsIgnoreCase("url")){
//                queryExpression
//                        .withKeyConditionExpression("urlText = :val")
//                        .withConsistentRead(false)
//                        .withIndexName("url-index");
//            }
//
//            List<T> answer = mapper.query(clazz, queryExpression);
//
//            return answer.size() > 0 ? clazz.cast(answer.get(0)) : null;
//        } catch (Exception e){
//            e.printStackTrace();
//            throw e;
//        }
//    }
//
//    public <T> QueryResultPage<T> queryList(String attribute, String value,
//                                               Class<T> clazz){
//        try {
//            DynamoDBMapper mapper = new DynamoDBMapper(client);
//            Map<String, AttributeValue> eav = new HashMap<>();
//            eav.put(":val", new AttributeValue().withS(value));
//
//            DynamoDBQueryExpression<T> queryExpression = new DynamoDBQueryExpression<T>()
//                    .withKeyConditionExpression(attribute+" = :val")
//                    .withExpressionAttributeValues(eav)
//                    .withScanIndexForward(false);
//
//            createQueryExpression(queryExpression, attribute);
//
//            QueryResultPage<T> qrp = mapper.queryPage(clazz, queryExpression);
//            return qrp;
//        } catch (Exception e){
//            e.printStackTrace();
//            throw e;
//        }
//    }
//
//    public <T> void createQueryExpression(DynamoDBQueryExpression<T> queryExpression, String attribute){
//        boolean globalIndex = false;
//        if (attribute.equalsIgnoreCase("url")){
//            queryExpression.withKeyConditionExpression("urlText = :val");
//            globalIndex = true;
//        } else if (attribute.equalsIgnoreCase("ownerEmail")){
//            queryExpression.withKeyConditionExpression("ownerEmail = :val");
//            globalIndex = true;
//        } else if (attribute.equalsIgnoreCase("contestId")){
//            queryExpression.withKeyConditionExpression("contestId = :val");
//            globalIndex = true;
//        }
//        if (globalIndex){
//            queryExpression
//                    .withConsistentRead(false)
//                    .withIndexName(attribute+"-index");
//        }
//    }
//
//    public <T> QueryResultPage<T> queryList(Class<T> clazz, String... keyValuePairs){
//        try {
//            DynamoDBMapper mapper = new DynamoDBMapper(client);
//            Map<String, AttributeValue> eav = new HashMap<>();
//
//            eav.put(":val0", new AttributeValue().withS(keyValuePairs[1]));
//            eav.put(":val2", new AttributeValue().withS(keyValuePairs[3]));
//
//            DynamoDBQueryExpression<T> queryExpression = new DynamoDBQueryExpression<T>()
//                    .withKeyConditionExpression("contestId"+" = :val0")
//                    .withFilterExpression("stateText"+" = :val2")
//                    .withExpressionAttributeValues(eav)
//                    .withIndexName("contestId"+"-index")
//                    .withScanIndexForward(false)
//                    .withConsistentRead(false);
//
//            QueryResultPage<T> qrp = mapper.queryPage(clazz, queryExpression);
//            return qrp;
//        } catch (Exception e){
//            e.printStackTrace();
//            throw e;
//        }
//    }
//
//    public <T> ScanResultPage<T> scanList(Class<T> clazz, Map<String,AttributeValue> lastEvaluatedPage,
//                                             String... keyValuePairs){
//        try {
//            DynamoDBMapper mapper = new DynamoDBMapper(client);
//            Map<String, AttributeValue> eav = new HashMap<>();
//            for (int i = 1; i < keyValuePairs.length; i+=2) {
//                eav.put(":val"+(i-1), new AttributeValue().withS(keyValuePairs[i]));
//            }
//
//            DynamoDBScanExpression scanExpression = new DynamoDBScanExpression()
//                    .withExpressionAttributeValues(eav);
//
//            String expression = "";
//            for (int i = 0; i < keyValuePairs.length; i+=2) {
//                expression += keyValuePairs[i] + " = :val" + i ;
//                if (i+2 < keyValuePairs.length)
//                    expression += " and ";
//            }
//            scanExpression.withFilterExpression(expression);
//
//            return mapper.scanPage(clazz, scanExpression);
//        } catch (Exception e){
//            e.printStackTrace();
//            throw e;
//        }
//    }
//
////    public static DBHandler getInstance(){
////        return instance;
////    }
//
//}
