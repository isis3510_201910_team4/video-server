const queue = require('./queue.js')
const util = require('util');
const {spawn} = require('child_process')
const exec = util.promisify(require('child_process').exec);
const initProcessTime = new Date().getTime();
const {promisify} = require('util');
const MAP_KEY = 'keysToUpdate';
const memjs = require('memjs')
let http = require('http');
let fs = require('fs');
let client = memjs.Client.create();
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

// let client = redis.createClient('redis://'+process.env.REDIS_HOST);

AWS = require('aws-sdk');
AWS.config.update({
  region: 'us-east-1',
  accessKeyId: process.env.AWS_ACCESS_KEY,
  secretAccessKey: process.env.AWS_ACCESS_SECRET,
});

// let ses = new AWS.SES({apiVersion: '2010-12-01', correctClockSkew:true});
let s3 = new AWS.S3();
let docClient = new AWS.DynamoDB.DocumentClient();


getAndProcess();

function getAndProcess() {
  try {
    queue.receiveMessage().then(data => {
      if (data == undefined) {
        console.log("No videos to process...");
        setTimeout(getAndProcess, 3000);
      } else {
        let videoId = data[0].Body;
        let attributes = data[0].MessageAttributes;
        let initialTime = new Date().getTime();
        let sourcePath = 'http://s3.amazonaws.com/smarttools-videos/raw/' + videoId;
        let destPath = 'videos/' + videoId + '.mp4';
        
        // let command = "ffmpeg -i " + sourcePath + " -preset fast -c:a aac -b:a 128k " +
        //   "-codec:v libx264 -b:v 1000k -minrate 500k -maxrate 2000k -bufsize 2000k " +
        //   destPath + " -hide_banner";
        
        download(sourcePath, 'videos/' + videoId, function () {
          let commandParams = [
            '-nostdin',
            '-loglevel',
            'panic',
            '-i',
            'videos/' + videoId,
            '-preset',
            'fast',
            destPath,
            '-nostats',
            '-y',
          ];
          
          const child = spawn('ffmpeg', commandParams, {stdio: 'ignore'});
          
          child.on('exit', code => {
            let urlVideo = process.env.HOST_VIDEO + attributes.contestUrl.StringValue;
            
            saveProcessedVideoS3(destPath, videoId).then(data => {
              let promises = [
                setContestAsProcessed(attributes.id.StringValue),
                // deleteLocalVideo(destPath),
                sendMailToUser(attributes.email.StringValue, urlVideo),
                updateRedis(attributes.contestId.StringValue)
              ];
              // if (process.env.SEND_MAIL) {
              //   console.log('e')
              //   promises.push(;
              // }
              Promise.all(promises).then(data => {
                setTimeout(getAndProcess, 1000);
              }).catch(promisesError => {
                console.log(promisesError);
                setTimeout(getAndProcess, 1000);
              });
            }).catch(error => {
              console.log(error);
            });
            
            logTimes(initialTime);
          });
        });
        
        
        // exec(command).then(commandResult => {
        //
        // }).catch(commandError => {
        //   console.log(commandError);
        // });// code = execSync(command);
      }
    }).catch(error => {
      console.log("Error getting messages out of the queue");
      setTimeout(getAndProcess, 3000);
      console.log(error)
    })
  } catch (e) {
    console.log(e);
  }
}

function saveProcessedVideoS3(videoPath) {
  return new Promise((resolve, reject) => {
    fs.readFile(videoPath, function (err, data) {
      if (err) throw err;
      let params = {
        ACL: "public-read",
        Body: data,
        Bucket: "smarttools-videos/converted",
        Key: videoPath.split('/')[1]
      };
      s3.putObject(params).promise().then(data => {
        resolve();
      }).catch(error => {
        console.log(error);
        reject(error);
      });
    });
  });
}

function updateRedis(contestId) {
  return new Promise((resolve, reject) => {
    client.get(MAP_KEY).then(data => {
      let obj = JSON.parse(data.value.toString());
      if (obj == null) obj = {};
      // obj['models.ContestSubmission-contestId-0b55a4b8-7486-490a-b5e4-2d2a99dd07eb'] = 1;
      obj['models.ContestSubmission-contestId-' + contestId] = 1;
      // obj['models.ContestSubmission-contestId-0b55a4b8-7486-490a-b5e4-2d2a99dd07eb-stateText-Processed'] = 1;
      obj['models.ContestSubmission-contestId-' + contestId + '-stateText-Processed'] = 1;
      client.set(MAP_KEY, JSON.stringify(obj), {expires: 600}).then(resp => {
        resolve();
      }).catch(error => {
        console.log(error);
        reject();
      });
    }).catch(err => {
      console.log(err);
    });
  });
}

function sendMailToUser(userEmail, urlVideo) {
  return new Promise((resolve, reject) => {
    const msg = {
      to: userEmail,
      from: 'c.hurtadoo@uniandes.edu.co',
      subject: 'Smarttools notification',
      html: "Dear client <br/>"
        + "Your video has been processed successfully! You can now watch it by clicking on the following link:<br/>"
        + "<a href='" + urlVideo + "'> Video link </a> <br/><br/>"
        + "Your Smart Tools Team"
    };
    sgMail.send(msg).then(()=>resolve()).catch(()=>reject());
  });
}

function setContestAsProcessed(contestSubmissionId) {
  return new Promise((resolve, reject) => {
    let params = {
      TableName: 'ContestSubmissions',
      Key: {"id": contestSubmissionId},
      UpdateExpression: "set stateText = :s",
      ExpressionAttributeValues: {":s": "Processed"},
      ReturnValues: "UPDATED_NEW"
    };
    
    docClient.update(params).promise().then(data => {
      resolve();
    }).catch(error => {
      console.error("Unable to update item. Error JSON:", JSON.stringify(error, null, 2));
      reject(error);
    });
  });
}

function deleteLocalVideo(videoPath) {
  return new Promise((resolve, reject) => {
    fs.unlink(videoPath, function (err, data) {
      if (err != null) {
        console.log(err);
        reject(err);
      }
      else
        resolve();
    });
  });
}

function logTimes(initialTime) {
  let timeDifference = new Date().getTime() - initialTime;
  console.log("Video processed in: " + (timeDifference / 1000));
  let timeDifference2 = new Date().getTime() - initProcessTime;
  console.log("Total time passed:" + (timeDifference2 / 1000));
}

function download(url, dest, cb) {
  let file = fs.createWriteStream(dest);
  let request = http.get(url, (response) => {
    response.pipe(file);
    file.on('finish', function () {
      file.close(cb);
    });
  });
}
